        </main>
        
        <footer class="footer bg-light py-3 mt-auto">
            <ul class="nav justify-content-center border-bottom pb-3 mb-3">
                <li class="nav-item">
                    <a href="https://getbootstrap.com/docs/5.2/examples/cheatsheet/" target="_blank" class="nav-link px-2 text-muted">Components</a>
                </li>
                <li class="nav-item">
                    <a href="https://getbootstrap.com/docs/5.2/helpers/clearfix/" target="_blank" class="nav-link px-2 text-muted">Helpers</a>
                </li>
                <li class="nav-item">
                    <a href="https://getbootstrap.com/docs/5.2/utilities/api/" target="_blank" class="nav-link px-2 text-muted">API</a>
                </li>
                <li class="nav-item">
                    <a href="https://icons.getbootstrap.com" target="_blank" class="nav-link px-2 text-muted">Icons</a>
                </li>
                <li class="nav-item">
                    <a href="https://getbootstrap.com/docs/5.2/examples/" target="_blank" class="nav-link px-2 text-muted">Examples</a>
                </li>
                <li class="nav-item">
                    <a href="https://themes.getbootstrap.com" target="_blank" class="nav-link px-2 text-muted">Themes</a>
                </li>
            </ul>
            <p class="text-center text-muted">© <?= date('Y'); ?> <?= get_bloginfo('name'); ?></p>
        </footer>

        <?php wp_footer(); ?>
        
        <script src="<?php echo get_template_directory_uri(); ?>/assets/js/app.js"></script>
    </body>
</html>