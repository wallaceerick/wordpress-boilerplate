<?php get_header(); ?>
    <section class="container pt-5 error h-100 d-flex flex-column justify-content-center align-items-center">
        <h1 class="mt-5 display-1"><?php _e('Erro 404'); ?></h1>
        <p class="lead"><?php _e('Nothing found for the requested page.'); ?></p>
        <a href="<?php echo get_home_url(); ?>" class="btn btn-primary"><?php _e('Try to go back home'); ?></a>
    </section>
<?php get_footer(); ?>