<article id="post-<?php the_ID(); ?>" <?php post_class('mt-5'); ?>>
    <div class="post__details">
        <?php the_post_thumbnail(); ?>
        <h3 class="display-5" itemprop="name headline"><?php the_title(); ?></h3>
        <p><?php the_excerpt(); ?></p>
        <a href="<?php the_permalink() ?>">Leia mais...</a>
    </div>
</article>