<img src="https://img.shields.io/badge/version-1.0.0-yellow.svg?cacheSeconds=2592000" />

# Wordpress Boilerplate
> A base project to fast development in Wordpress.

## Installation
- Clone this repository
- Import [database.sql](database.sql) and update [wp-config.php](wp-config.php)
- Access `Settings > Permalink` in `wp-admin` and save to generate `.htaccess` file.

## Development
Go to theme folder in `wp-content/themes/boilerplate` and follow [this steps](wp-content/themes/boilerplate/README.md).

## Login
Go to `wp-admin` and use this credentials.
```sh
wallace.erick
1IIvjzA3f(3Y61XOr)U$t8pB
```

## Author

👤 [**Wallace Erick**](https://gitlab.com/wallaceerick)
