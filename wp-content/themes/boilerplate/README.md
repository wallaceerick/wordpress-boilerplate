<img src="https://img.shields.io/badge/version-1.0.0-yellow.svg?cacheSeconds=2592000" />

# Theme
> A blank theme to optimize development in Wordpress. 

## Requirements

-   [Node](https://nodejs.org/en/)
-   [Gulp](https://gulpjs.com/)
-   [NPM](https://www.npmjs.com/)

## Dependencies

```js
npm install 
// To install all project dependencies
```

## Tasks

```js
npm start or gulp
// To start the server 

npm build or gulp build --production 
// To generate all production files 

npm generate or gulp assets
// To generate only assets files
```
