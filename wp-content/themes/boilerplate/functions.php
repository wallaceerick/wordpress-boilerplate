<?php
    // Custom more link
    // function read_more_link() {
    //     if (!is_admin()) {
    //         return ' <a href="' . esc_url(get_permalink()) . '" class="more-link">...</a>';
    //     }
    // }
    // add_filter('the_content_more_link', 'read_more_link');

    // Custom read more link
    // function excerpt_read_more_link($more) {
    //     if (!is_admin()) {
    //         global $post;
    //         return ' <a href="' . esc_url(get_permalink($post->ID)) . '" class="more-link">...</a>';
    //     }
    // }
    // add_filter('excerpt_more', 'excerpt_read_more_link');

    // function filter_ptags_on_images($content){
    //     return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
    // }
    // add_filter('the_content', 'filter_ptags_on_images');

    // Custom admin color scheme
    function admin_color_scheme() { 
        $theme_dir = get_stylesheet_directory_uri(); 
        wp_admin_css_color('custom_admin', __( 'Custom' ),
        $theme_dir . '/admin.css',
            array('#712cf9', '#4c0bce', '#000', '#fff')
        );
    }
    add_action('admin_init', 'admin_color_scheme');
    
    // Remove width and height of post_thumbnail
    function remove_thumbnail_dimensions( $html, $post_id, $post_image_id ) {
        $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
        return $html;
    }
    add_filter( 'post_thumbnail_html', 'remove_thumbnail_dimensions', 10, 3);

    // Add a Custom Menu in Header
    function custom_header_menu() {
        register_nav_menu('header-menu', 'Header');
    }
    add_action('init', 'custom_header_menu');

    // Add Description to Menu Links
    function prefix_nav_description($item_output, $item, $depth, $args) {
        if ( !empty( $item->description ) ) {
            $item_output = str_replace($args->link_after . '</a>', '<span class="menu-item-description">' . $item->description . '</span>' . $args->link_after . '</a>', $item_output);
        }
        return $item_output;
    }
    add_filter('walker_nav_menu_start_el', 'prefix_nav_description', 10, 4); 

    // Add thumbnails support for posts
    function initial_setup() {
        add_theme_support('post-thumbnails');
    } 
    add_action('after_setup_theme', 'initial_setup');
    
    // Change the default title separator
    function title_separator($sep) {
        $sep = '|';
        return $sep;
    }
    add_filter('document_title_separator', 'title_separator');

    // Create custom sidebar
    function widgets_init() {
        register_sidebar( array(
            'name'          => 'Sidebar',
            'id'            => 'primary-sidebar',
            'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
            'after_widget'  => '</li>',
            'before_title'  => '<h3 class="widget-title">',
            'after_title'   => '</h3>',
        ));
    }
    add_action('widgets_init', 'widgets_init');

    // Remove unused configs
    function remove_ununsed_config() {
        // Metatag
        remove_action('wp_head', 'wp_generator');

        // Emoji
        remove_action('wp_head', 'print_emoji_detection_script', 7);
        remove_action('wp_print_styles', 'print_emoji_styles');
        remove_action('admin_print_scripts', 'print_emoji_detection_script');
        remove_action('admin_print_styles', 'print_emoji_styles');

        // Scripts
        remove_action('wp_enqueue_scripts', 'generate_color_scripts', 50);
        remove_action('wp_enqueue_scripts', 'generate_spacing_scripts', 50);
        remove_action('wp_enqueue_scripts', 'generate_typography_scripts', 50);
        remove_action('wp_enqueue_scripts', 'generate_secondary_color_scripts', 80);
        remove_action('wp_enqueue_scripts', 'generate_background_scripts', 70);
    }
    add_action('after_setup_theme','remove_ununsed_config');

    // Custom breadcrumbs
    function custom_breadcrumbs() {
        
        // Configuracoes
        $breadcrums_class   = 'breadcrumb';
        $home_title         = 'Home';
        
        // Se você tiver algum tipo de postagem personalizado com taxonomias personalizadas, coloque o nome da taxonomia abaixo (e.g. product_cat)
        $custom_taxonomy    = 'product_cat';
        
        // Obter as informações de consulta e publicação
        global $post,$wp_query;
        
        // Não exibir na página inicial
        if ( !is_front_page() ) {
        
            // Construa o breadcrumbs
            echo '<ol class="' . $breadcrums_class . '" itemscope itemtype="https://schema.org/BreadcrumbList">';
            
            // Home page
            echo '<li class="breadcrumb-item item-home" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">';
                echo '<a class="bread-link bread-home" href="' . get_home_url() . '"><span itemprop="name">' . $home_title . '</span></a>';
                echo '<meta itemprop="position" content="1">';
            echo '</li>';

            
            if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {
                echo '<li class="breadcrumb-item item-current item-archive" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">';
                    echo '<span class="bread-current bread-archive" itemprop="name">' . post_type_archive_title($prefix, false) . '</span>';
                    echo '<meta itemprop="position" content="2">';
                echo '</li>';
                
            } else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {
                
                // Se post é um tipo de postagem personalizado
                $post_type = get_post_type();
                
                // Se for um nome e link de exibição de tipo de postagem personalizado
                if($post_type != 'post') {
                    
                    $post_type_object = get_post_type_object($post_type);
                    $post_type_archive = get_post_type_archive_link($post_type);
                
                    echo '<li class="breadcrumb-item item-cat item-custom-post-type-' . $post_type . '"  itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '" itemprop="name"   >' . $post_type_object->labels->name . '</a>';
                        echo '<meta itemprop="position" content="2">';
                    echo '</li>';
                
                }
                
                $custom_tax_name = get_queried_object()->name;
                echo '<li class="breadcrumb-item item-current item-archive"  itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><span class="bread-current bread-archive" itemprop="name">' . $custom_tax_name . '</span>';
                    echo '<meta itemprop="position" content="2">';
                echo '</li>';
                
            } else if ( is_single() ) {
                
                $post_type = get_post_type();
                
                if($post_type != 'post') {
                    
                    $post_type_object = get_post_type_object($post_type);
                    $post_type_archive = get_post_type_archive_link($post_type);
                
                    echo '<li class="breadcrumb-item item-cat item-custom-post-type-' . $post_type . '"  itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '" itemprop="name">' . $post_type_object->labels->name . '</a>';
                        echo '<meta itemprop="position" content="2">';
                    echo '</li>';
                
                }
                
                // Obter informações de categoria
                $category = get_the_category();
                
                if(!empty($category)) {
                
                    // A última publicação da categoria está em
                    $last_category = end(array_values($category));
                    
                    // Obter pai de qualquer categoria
                    $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                    $cat_parents = explode(',',$get_cat_parents);
                    
                    // Loop através de categorias pai e armazenar em variável $ cat_display
                    $cat_display = '';
                    foreach($cat_parents as $parents) {
                        $cat_display .= '<li class="breadcrumb-item item-cat" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><span  itemprop="name">'.$parents.'</span><meta itemprop="position" content="2"></li>';          
                    }
                
                }
                
                // Se for um tipo de publicação personalizado dentro de uma taxonomia personalizada
                $taxonomy_exists = taxonomy_exists($custom_taxonomy);
                if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {
                    
                    $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
                    $cat_id         = $taxonomy_terms[0]->term_id;
                    $cat_nicename   = $taxonomy_terms[0]->slug;
                    $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                    $cat_name       = $taxonomy_terms[0]->name;
                
                }
                
                // Verifique se o post está em uma categoria
                if(!empty($last_category)) {
                    echo $cat_display;
                    echo '<li class="breadcrumb-item item-current item-' . $post->ID . '" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><span class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '" itemprop="name">' . get_the_title() . '</span>';
                        echo '<meta itemprop="position" content="2">';
                    echo '</li>';
                    
                // Em caso de publicação em uma taxonomia personalizada
                } else if(!empty($cat_id)) {
                    
                    echo '<li class="breadcrumb-item item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_link . '" title="' . $cat_name . '" itemprop="name">' . $cat_name . '</a>';
                        echo '<meta itemprop="position" content="2">';
                    echo '</li>';
                    echo '<li class="breadcrumb-item item-current item-' . $post->ID . '" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><span class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '" itemprop="name">' . get_the_title() . '</span>';
                        echo '<meta itemprop="position" content="3">';
                    echo '</li>';
                
                } else {
                    
                    echo '<li class="breadcrumb-item item-current item-' . $post->ID . '" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><span class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '" itemprop="name">' . get_the_title() . '</span>';
                        echo '<meta itemprop="position" content="2">';
                    echo '</li>';
                    
                }
                
            } else if ( is_category() ) {
                
                // Página Category
                echo '<li class="breadcrumb-item item-current item-cat" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><span class="bread-current bread-cat" itemprop="name">' . single_cat_title('', false) . '</span>';
                    echo '<meta itemprop="position" content="2">';
                echo '</li>';
                
            } else if ( is_page() ) {
                // var_dump(wp_get_post_parent_id($post->ID));
                // Página padrão
                if(wp_get_post_parent_id($post->ID)){
                    $anc = get_post_ancestors( $post->ID );                 

                    $anc = array_reverse($anc);                   

                    if ( !isset( $parents ) ) $parents = null;
                    foreach ( $anc as $ancestor ) {
                        $parents .= '<li class="breadcrumb-item item-parent item-parent-' . $ancestor . '" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '" itemprop="name">' . get_the_title($ancestor) . '</a><meta itemprop="position" content="2"></meta>';
                    }
                    
                    echo $parents;
                    
                    // Página Atual
                    echo '<li class="breadcrumb-item item-current item-' . $post->ID . '" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><span title="' . get_the_title() . '" itemprop="name"> ' . get_the_title() . '</span><meta itemprop="position" content="3"></li>';
                    
                } else {
                    
                    // Basta exibir a página atual se não os pais
                    echo '<li class="breadcrumb-item item-current item-' . $post->ID . '" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><span class="bread-current bread-' . $post->ID . '" itemprop="name"> ' . get_the_title() . '</span><meta itemprop="position" content="2"></li>';
                    
                }
                
            } else if ( is_tag() ) {
                
                // Página de Tag
                
                // Obter informações de tag
                $term_id        = get_query_var('tag_id');
                $taxonomy       = 'post_tag';
                $args           = 'include=' . $term_id;
                $terms          = get_terms( $taxonomy, $args );
                $get_term_id    = $terms[0]->term_id;
                $get_term_slug  = $terms[0]->slug;
                $get_term_name  = $terms[0]->name;
                
                // Exibir o nome da Tag
                echo '<li class="breadcrumb-item item-current item-tag-' . $get_term_id . ' item-tag-' . $get_term_slug . '" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><span class="bread-current bread-tag-' . $get_term_id . ' bread-tag-' . $get_term_slug . '" itemprop="name">' . $get_term_name . '</span>';
                    echo '<meta itemprop="position" content="2">';
                echo '</li>';
            
            } elseif ( is_day() ) {
                
                // Day archive
                
                // Year link
                echo '<li class="breadcrumb-item item-year item-year-' . get_the_time('Y') . '" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '" itemprop="name">' . get_the_time('Y') . ' Archives</a>';
                    echo '<meta itemprop="position" content="2">';
                echo '</li>';
                
                // Month link
                echo '<li class="breadcrumb-item item-month item-month-' . get_the_time('m') . '" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '" itemprop="name">' . get_the_time('M') . ' Archives</a>';
                    echo '<meta itemprop="position" content="2">';
                echo '</li>';
                
                // Day display
                echo '<li class="breadcrumb-item item-current item-' . get_the_time('j') . '" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><span class="bread-current bread-' . get_the_time('j') . '" itemprop="name"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</span>';
                echo '<meta itemprop="position" content="2">';
            echo '</li>';
                
            } else if ( is_month() ) {
                
                // Arquivo               

                echo '<li class="breadcrumb-item item-year item-year-' . get_the_time('Y') . '" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '" itemprop="name">' . get_the_time('Y') . ' Archives</a>';
                    echo '<meta itemprop="position" content="2">';
                echo '</li>';
                

                echo '<li class="breadcrumb-item item-month item-month-' . get_the_time('m') . '" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><span class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '" itemprop="name">' . get_the_time('M') . ' Archives</span>';
                    echo '<meta itemprop="position" content="2">';
                echo '</li>';
                
            } else if ( is_year() ) {
                

                echo '<li class="breadcrumb-item item-current item-current-' . get_the_time('Y') . '" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><span class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '" itemprop="name">' . get_the_time('Y') . ' Archives</span>';
                    echo '<meta itemprop="position" content="2">';
                echo '</li>';
                
            } else if ( is_author() ) {
                
                // Autor
                
                // Get the author information
                global $author;
                $userdata = get_userdata( $author );
                

                echo '<li class="breadcrumb-item item-current item-current-' . $userdata->user_nicename . '" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><span class="bread-current bread-current-' . $userdata->user_nicename . '" title="' . $userdata->display_name . '" itemprop="name">' . 'Author: ' . $userdata->display_name . '</span>';
                    echo '<meta itemprop="position" content="2">';
                echo '</li>';
            
            } else if ( get_query_var('paged') ) {
                

                echo '<li class="breadcrumb-item item-current item-current-' . get_query_var('paged') . '" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><span class="bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '" itemprop="name">'.__('Page') . ' ' . get_query_var('paged') . '</span>';
                    echo '<meta itemprop="position" content="2">';
                echo '</li>';
                
            } else if ( is_search() ) {
            
                // Página Search
                echo '<li class="breadcrumb-item item-current item-current-' . get_search_query() . '" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><span class="bread-current bread-current-' . get_search_query() . '" title="Resultado da pesquisa por: ' . get_search_query() . '" itemprop="name">Resultado da pesquisa por: ' . get_search_query() . '</span>';
                    echo '<meta itemprop="position" content="2">';
                echo '</li>';
            
            } elseif ( is_404() ) {
                
                // Pagina 404
                echo '<li class="breadcrumb-item item-error" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><span itemprop="name">' . 'Página não encontrada' . '</span><meta itemprop="position" content="2"></li>';
            } 
            elseif ( is_home() ) {
                
                // Pagina 404
                echo '<li class="breadcrumb-item item-error" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><span itemprop="name">' .  esc_html('Blog') . '</span><meta itemprop="position" content="2"></li>';
            }
            echo '</ol>';
            
        }
        
    }
    
    // Custom query to return posts
    function smart_query($args, $template) {
        $query = new WP_Query($args);
        if ($query->have_posts()) {
            while ($query->have_posts()) : $query->the_post();
            // get_template_part('includes/block-post');
            get_template_part($template);
            endwhile;
        }
        wp_reset_query();
    }

    // Random number to force cache expires
    function random_number() {
        return rand(0000000, 9999999);
    }
    
    /*
    // Compress HTML
    class FLHM_HTML_Compression {
        protected $flhm_compress_css = true;
        protected $flhm_compress_js = true;
        protected $flhm_info_comment = true;
        protected $flhm_remove_comments = true;
        protected $html;
        public function __construct($html) {
            if (!empty($html)) {
                $this->flhm_parseHTML($html);
            }
        }
        
        public function __toString() {
            return $this->html;
        }
        
        protected function flhm_bottomComment($raw, $compressed) {
            $raw = strlen($raw);
            $compressed = strlen($compressed);
            $savings = ($raw-$compressed) / $raw * 100;
            $savings = round($savings, 2);
            return '<!-- HTML compressed, size saved '. $savings . '%. From '.$raw.' bytes, now ' . $compressed . ' bytes-->';
        }
        
        protected function flhm_minifyHTML($html) {
            $pattern = '/<(?<script>script).*?<\/script\s*>|<(?<style>style).*?<\/style\s*>|<!(?<comment>--).*?-->|<(?<tag>[\/\w.:-]*)(?:".*?"|\'.*?\'|[^\'">]+)*>|(?<text>((<[^!\/\w.:-])?[^<]*)+)|/si';
            preg_match_all($pattern, $html, $matches, PREG_SET_ORDER);
            $overriding = false;
            $raw_tag = false;
            $html = '';
            foreach ($matches as $token) {
                $tag = (isset($token['tag'])) ? strtolower($token['tag']) : null;
                $content = $token[0];
                if (is_null($tag)) {
                    if (!empty($token['script'])) {
                        $strip = $this->flhm_compress_js;
                    }
                    else if (!empty($token['style'])) {
                        $strip = $this->flhm_compress_css;
                    }
                    else if ($content == '<!-- wp-html-compression no compression -->'){
                        $overriding = !$overriding; 
                        continue;
                    }
                    else if ($this->flhm_remove_comments) {
                        if (!$overriding && $raw_tag != 'textarea') {
                            $content = preg_replace('/<!--(?!\s*(?:\[if [^\]]+]|<!|>))(?:(?!-->).)*-->/s', '', $content);
                        }
                    }
                }
                else {
                    if ($tag == 'pre' || $tag == 'textarea') {
                        $raw_tag = $tag;
                    }
                    else if ($tag == '/pre' || $tag == '/textarea') {
                        $raw_tag = false;
                    }
                    else {
                        if ($raw_tag || $overriding) {
                            $strip = false;
                        }
                        else {
                            $strip = true; 
                            $content = preg_replace('/(\s+)(\w++(?<!\baction|\balt|\bcontent|\bsrc)="")/', '$1', $content); 
                            $content = str_replace(' />', '/>', $content);
                        }
                    }
                } 
                
                if ($strip) {
                    $content = $this->flhm_removeWhiteSpace($content);
                }
                $html .= $content;
            } 
            
            return $html;
        } 
        
        public function flhm_parseHTML($html) {
            $this->html = $this->flhm_minifyHTML($html);
            if ($this->flhm_info_comment) {
                $this->html .= "\n" . $this->flhm_bottomComment($html, $this->html);
            }
        }
        
        protected function flhm_removeWhiteSpace($str) {
            $str = str_replace("\t", ' ', $str);
            $str = str_replace("\n",  '', $str);
            $str = str_replace("\r",  '', $str);
            while (stristr($str, '  ')) {
                $str = str_replace('  ', ' ', $str);
            }   
            return $str;
        }
    }
    function html_compression_finish($html) {
        return new FLHM_HTML_Compression($html);
    }
    function html_compression_start() {
        ob_start('html_compression_finish');
    }
    add_action('get_header', 'html_compression_start');
    */ 
    