const 
    gulp = require('gulp'),
    { parallel, series } = require('gulp')

const
    tinypng        = require('gulp-tinypng-compress'),
    uglify         = require('gulp-uglify'),
    sass           = require('gulp-sass')(require('sass')),
    sourcemaps     = require('gulp-sourcemaps'),
    concat         = require('gulp-concat'),
    browser        = require('browser-sync').create(),
    autoprefixer   = require('gulp-autoprefixer'),
    babel          = require('gulp-babel'),
    header         = require('gulp-header-comment')

const 
    source = {
        css: './assets/css/',
        js: './assets/js/',
        svg: './assets/svg/',
        fonts: './assets/fonts/',
        images: './assets/images/'
    },
    banner = `
                Theme Name: <%= pkg.title %>
                Theme URI: <%= pkg.homepage %>
                Author: <%= pkg.author.name %> <<%= pkg.author.email %>>
                Author URI: <%= pkg.author.url %>
                Description: <%= pkg.description %>
                Version: <%= pkg.version %>
                Text Domain: <%= pkg.name %>
                Lastest Update: <%= moment().format('DD/MM/YY') %>
            `

function images(cb) {
    gulp.src(source.images + '**/*.{png,jpg,jpeg}')
        .pipe(tinypng({
            key: '8nJXfZZX22V6hdL4vmYmWwV9nHzFB0Pb',
            sigFile: 'images/.tinypng-sigs',
            log: true
        }))
        .pipe(gulp.dest(source.images))
    cb()
}

function js(cb) {
    gulp.src([`!${source.js}app.js`, './node_modules/bootstrap/dist/js/bootstrap.bundle.min.js', `${source.js}*js`])
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['@babel/preset-env']
        }))
        .pipe(concat('app.js'))
        .pipe(uglify())
        .pipe(header(banner))
        .pipe(sourcemaps.write('../maps'))
        .pipe(gulp.dest(source.js))
        .pipe(browser.stream())
    cb()
}

function css(cb) {
    gulp.src(`${source.css}**/*.sass`)
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'compressed'
        })
        .on('error', sass.logError))
        .pipe(autoprefixer({
            browserlist: ['last 2 versions'],
            cascade: false
        }))
        .pipe(header(banner))
        .pipe(sourcemaps.write('./assets/maps'))
        .pipe(gulp.dest('./'))
        .pipe(browser.stream())
    cb()
}

function fonts(cb) {
    gulp.src(['./node_modules/bootstrap-icons/font/fonts/**/*'])
        .pipe(gulp.dest(source.fonts))
    cb()
}

function icons(cb) {
    gulp.src(['./node_modules/bootstrap-icons/bootstrap-icons.svg'])
        .pipe(gulp.dest(source.images))
    cb()
}

function watcher() {
    browser.init({
        proxy: 'http://localhost/testes/wordpress-boilerplate/',
        // server: {
        //     baseDir: dist
        // }
    })
    gulp.watch(`${source.css}**/*.sass`, css)
    gulp.watch(`${source.js}**/*.js`, js).on('change', browser.reload)
    gulp.watch('**/*.php').on('change', browser.reload)
}

exports.default = series(css, js, fonts, icons, watcher)
exports.build = parallel(css, js, fonts, icons)
exports.assets = parallel(css, js, fonts, icons)
exports.images = parallel(images)
exports.css = parallel(css)
