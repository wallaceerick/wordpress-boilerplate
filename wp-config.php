<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');  

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '9+@}<@m>]+fwOg|><K<X~dK}%,m6:y`}6`?ppEc|exd:)aq$:42_|6??@~_0Z-|@');
define('SECURE_AUTH_KEY',  'Q8onamC)Q%|{:8Iia#-LDxfJLHeV<i/R:W[bgPrpK~Pyx3DOb_IB #iC#]QNz@Uk');
define('LOGGED_IN_KEY',    '<5F4k(5|i!^6o]oHAvWO2M{-zXxr%T8ppYEBy(XZ`RA<g(@>Ty0D*dXls/D1h#w{');
define('NONCE_KEY',        '4H(B@RGA9hlt)|-)1sY-dc},% AUGM},NK~2~)HlG-8MiEl]y|! E?;YA .fJ1u,');
define('AUTH_SALT',        '$:v5v}Eq2-1.=)4->q]-,$Y/kevfdd0 @<mHE@V&V&s/<Rlv/F`2~jzZL~G{W#jd');
define('SECURE_AUTH_SALT', '/U0#}|:>F]oEdvlK-r9ziy~WH|py,tOGPJ_=7LoGmO_3>f/w =[N}r+$[!-cd5&*');
define('LOGGED_IN_SALT',   '^#a:B3W+uP7+|L=k=S/NU#Rlg`Z,bN3:m])ka>$.,+Hwn+*-t_Ivo =sQiq%v(2|');
define('NONCE_SALT',       '(twL*TxU/idl![T-m+ZW^nLF`6Oy|>/WTxx=y_;1GN R3)j-L$O6;pK8}eN>MnX9');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define('WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined('ABSPATH' ) ) {
	define('ABSPATH', __DIR__ . '/');
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
