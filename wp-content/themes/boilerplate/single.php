<?php get_header(); ?>
    <div class="container pt-5 mt-5">
        <?php custom_breadcrumbs(); ?>
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <h1 class="display-1"><?php the_title(); ?></h1>
                <?php the_content(); ?>
                <?php the_tags(); ?>
            </article>
        <?php endwhile; endif; ?>
    </div>
<?php get_footer(); ?>