<?php get_header(); ?>
    <div class="container pt-5">
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <article id="post-<?php the_ID(); ?>" class="mt-5">
                <?php custom_breadcrumbs(); ?>
                <div class="row">
                    <div class="col-12 order-2 order-md-1">
                        <header>
                        <h1 class="display-1"><?php the_title(); ?></h1>
                        </header>
                        <div class="content__wrapper">
                            <?php the_content(); ?>
                        </div>
                    </div>
                    <div class="col-12 order-1 order-md-2 text-left text-md-right image-pages">
                        <?php the_post_thumbnail(); ?>
                    </div>
                </div>
            </article>

            <?php if(get_field('posts')) : ?>
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-6 mb-4 mb-md-0">
                            <h6>Podcast Insights</h6>
                        </div>
                        <div class="col-12 col-md-6">
                            <?php 
                                smart_query(array(
                                    'post_type' => 'post',
                                    'posts_per_page' => 1,
                                    'ignore_sticky_posts' => 1,
                                    'order' => 'DESC'
                                ),  'includes/block-post');
                            ?>
                        </div>
                    </div>
                </div>
                <?php wp_reset_postdata(); ?>
            <?php endif; ?>

        <?php endwhile; endif; ?>
    </div>
<?php get_footer(); ?>