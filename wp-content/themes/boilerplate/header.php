<!DOCTYPE html>
<html <?php language_attributes(); ?> class="h-100">

    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><?php wp_title('&bull;', true, 'right'); echo get_bloginfo('name'); ?></title>
        
        <meta name="author" content="Wallace Erick">
        <meta name="robots" content="index, follow">
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="application-name" content="<?php echo get_bloginfo('name'); ?>">
        <meta name="theme-color" content="#712cf9">
        <meta name="msapplication-navbutton-color" content="#712cf9">
        <meta name="apple-mobile-web-app-status-bar-style" content="#712cf9">
        <meta name="apple-mobile-web-app-title" content="#712cf9">
        <meta name="msapplication-TileColor" content="#712cf9" />
        <meta name="theme-color" content="#712cf9" />

        <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/apple-touch-icon.png" sizes="180x180">
        <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/favicon-32x32.png" sizes="32x32" type="image/png">
        <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/favicon-16x16.png" sizes="16x16" type="image/png">
        <link rel="mask-icon" hre
        f="<?php echo get_template_directory_uri(); ?>/assets/images/icons/safari-pinned-tab.svg" color="#712cf9">
        <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/favicon.ico">
        <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/manifest.json">
        
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800&display=swap">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">
        
        <?php wp_head(); ?>
    </head>

    <body <?php body_class('d-flex flex-column h-100'); ?>>

        <header>
            <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
                <div class="container">
                    <h1 class="d-flex align-items-center mb-0 mb-md-0 me-md-auto">
                        <a href="<?php echo get_home_url(); ?>" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-white text-decoration-none" alt="<?php echo get_bloginfo('name'); ?>">
                            <svg class="bi me-2" width="40" height="32">
                                <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/bootstrap-icons.svg#bootstrap"></use>
                            </svg>
                        </a>
                    </h1>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navigation" aria-controls="navigation" aria-expanded="false" aria-label="Menu">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div id="navigation" class="collapse navbar-collapse">
                        <?php
                            wp_nav_menu(array(
                                'menu_class' => 'navbar-nav ms-auto mb-2 mb-md-0',
                                'theme_location' => 'header-menu', 
                                'container' => 'ul'
                            )); 
                        ?>
                    </div>
                </div>
            </nav>
        </header>
        
        <main class="flex-shrink-0">