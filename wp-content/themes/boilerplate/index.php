<?php get_header(); ?>    
    <div class="container pt-5 mt-5">
        <?php custom_breadcrumbs(); ?>
        <h1 class="display-1">Blog</h1>
        <?php
            smart_query(array(
                'post_type'    => 'post',
                'order'        => 'DESC',
                'post__not_in' => get_option('sticky_posts'),
            ),  'includes/block-posts');
        ?>
    </div>
<?php get_footer(); ?>